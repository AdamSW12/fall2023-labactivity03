package linearalgebra;

public class Vector3D {
    private double x;
    private double y;
    private double z;
    
    
    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    public double getZ() {
        return z;
    }

    public double magnitude(){
        Double magnitude = Math.sqrt((x * x) + (y*y) + (z*z));
        return magnitude;
    }

    public double dotProduct(Vector3D vectorY){
        double dot = (this.x * vectorY.getX()) + (this.y * vectorY.getY()) + (this.z * vectorY.getZ());
        return dot;
    }

    public Vector3D add(Vector3D vectorY){
        Vector3D vectorZ = new Vector3D(this.x + vectorY.getX(),this.y + vectorY.getY(),this.z + vectorY.getZ());
        return vectorZ;
    }
}
