package linearalgebra;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Vector3DTest {
    
    @Test
    public void Vector3D_TestGetMethods(){
        Vector3D testVector = new Vector3D(1, 2, 3);

        assertEquals("Checking if the getter for x returns 1", 1, testVector.getX(),0.1);
        assertEquals("Checking if the getter for y returns 2", 2, testVector.getY(),0.1);
        assertEquals("Checking if the getter for z returns 3", 3, testVector.getZ(),0.1);
    }

    @Test
    public void Magnitude_returns3dot74(){
        Vector3D testVector = new Vector3D(1, 2, 3);

        assertEquals("Checking magnitude", 3.7316, testVector.magnitude(), 0.6);
    }

    @Test
    public void dotProduct_returns13(){
        Vector3D VectorX = new Vector3D(1, 1, 2);
        Vector3D VectorY = new Vector3D(2, 3, 4);

        assertEquals("checking dot product returns 13", 13, VectorX.dotProduct(VectorY), 0.5);
    }

    @Test
    public void add_returns346(){
        Vector3D vectorX = new Vector3D(1, 1, 2);
        Vector3D vectorY = new Vector3D(2, 3, 4);

        Vector3D vectorZ = vectorX.add(vectorY);

        assertEquals("Checking if the getter for x returns 3 after adding", 3, vectorZ.getX(),0.1);
        assertEquals("Checking if the getter for y returns 4 after adding", 4, vectorZ.getY(),0.1);
        assertEquals("Checking if the getter for z returns 6 after adding", 6, vectorZ.getZ(),0.1);

    }


}
